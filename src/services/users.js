const knex = require('../knex');
const fs = require('fs');

const USER_TABLE = 'users';

async function uploadAvatar(image) {
  const base64Data = image.split(',')[1];
  const imageFormat = image.slice(image.indexOf('/') + 1, image.indexOf(';'));
  const imageName = `./public/avatars/${Date.now()}.${imageFormat}`;
  fs.writeFile(imageName, base64Data, 'base64', (err) => {
    if (err) {
      return err;
    }
    return 'Avatar uploaded successfully';
  });
  return imageName;
}

module.exports = {
  async getById(id) {
    const item = await knex(USER_TABLE).select('*').where({ id }).first();
    if (item.avatar !== null) {
      item.avatar = `http://localhost:3000/${item.avatar.split('/')[3]}`;
    }
    return item;
  },

  async getList() {
    const item = await knex(USER_TABLE).select('*');

    return item;
  },

  async addItem(item) {
    if (item.avatar) {
      item.avatar = await uploadAvatar(item.avatar);
    }
    return knex(USER_TABLE).insert(item);
  },

  async updateItem(id, item) {
    const existingAvatar = await knex(USER_TABLE).select('*').where({ id }).first();

    if (item.avatar) {
      try {
        fs.unlinkSync(existingAvatar.avatar);

        item.avatar = await uploadAvatar(item.avatar);
      } catch (err) {
        return err;
      }
    }
    const updated = await knex(USER_TABLE)
      .update({
        username: item.username || null,
        email: item.email || null,
        avatar: item.avatar || null,
        age: item.age || null,
        phone: item.phone || null,
      })
      .where({ id });

    return updated;
  },

  async patchItem(id, item) {
    if (item.avatar) {
      try {
        const existingAvatar = await knex(USER_TABLE).select('*').where({ id }).first();

        fs.unlinkSync(existingAvatar.avatar);

        item.avatar = await uploadAvatar(item.avatar);
      } catch (err) {
        return err;
      }
    }

    const updated = await knex(USER_TABLE).update(item).where({ id });
    return updated;
  },

  async removeItem(id) {
    const item = await knex(USER_TABLE).select('*').where({ id }).first();

    const deleted = knex(USER_TABLE).where({ id }).del();

    if (deleted) {
      try {
        fs.unlinkSync(item.avatar);
      } catch (err) {
        return err;
      }
    }
    return deleted;
  },
};
