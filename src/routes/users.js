const express = require('express');

const { getById, getList, addItem, updateItem, patchItem, removeItem } = require('../services/users');

const router = express.Router();

const PREFIX = '/users';

router.get('/:id', async (req, res) => {
  const user = await getById(req.params.id);

  if (!user) {
    res.sendStatus(404);
    return;
  }

  res.send(user);
});

router.get('/', async (req, res) => {
  const users = await getList();
  if (!users) {
    res.sendStatus(404);
    return;
  }

  res.send(users);
});

router.post('/', async (req, res) => {
  try {
    await addItem(req.body);
    res.sendStatus(201);
  } catch (error) {
    res.sendStatus(400);
  }
});

router.put('/:id', async (req, res) => {
  try {
    await updateItem(req.params.id, req.body);
    res.sendStatus(200);
  } catch (error) {
    res.sendStatus(404);
  }
});

router.patch('/:id', async (req, res) => {
  try {
    await patchItem(req.params.id, req.body);
    res.sendStatus(200);
  } catch (error) {
    res.sendStatus(404);
  }
});

router.delete('/:id', async (req, res) => {
  try {
    await removeItem(req.params.id);
    res.sendStatus(200);
  } catch (error) {
    res.sendStatus(400);
  }
});

module.exports = [PREFIX, router];
